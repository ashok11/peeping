1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="tv.mta.flutter_playout" >
4
5    <uses-sdk android:minSdkVersion="16" />
5-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml
6
7    <uses-permission android:name="android.permission.INTERNET" />
7-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:4:5-66
7-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:4:22-64
8    <uses-permission android:name="android.permission.WAKE_LOCK" />
8-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:6:5-68
8-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:6:22-65
9
10    <application>
10-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:8:5-37:19
11        <service
11-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:9:9-16:19
12            android:name="tv.mta.flutter_playout.audio.AudioService"
12-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:9:18-52
13            android:enabled="true"
13-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:10:13-35
14            android:exported="false"
14-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:11:13-37
15            android:stopWithTask="false" >
15-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:12:13-41
16            <intent-filter>
16-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:13:13-15:29
17                <action android:name="android.intent.action.MEDIA_BUTTON" />
17-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:14:17-77
17-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:14:25-74
18            </intent-filter>
19        </service>
20        <service
20-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:18:9-22:19
21            android:name="tv.mta.flutter_playout.MediaNotificationManagerService"
21-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:19:13-60
22            android:exported="false"
22-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:20:13-37
23            android:stopWithTask="false" >
23-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:21:13-41
24        </service>
25
26        <receiver android:name="tv.mta.flutter_playout.audio.RemoteReceiver" >
26-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:24:9-29:20
26-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:25:13-49
27            <intent-filter>
27-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:13:13-15:29
28                <action android:name="android.intent.action.MEDIA_BUTTON" />
28-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:14:17-77
28-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:14:25-74
29            </intent-filter>
30        </receiver>
31        <receiver android:name="tv.mta.flutter_playout.video.RemoteReceiver" >
31-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:31:9-36:20
31-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:32:13-49
32            <intent-filter>
32-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:13:13-15:29
33                <action android:name="android.intent.action.MEDIA_BUTTON" />
33-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:14:17-77
33-->/home/durgesh/sdks/flutter/.pub-cache/hosted/pub.dartlang.org/flutter_playout-1.0.41/android/src/main/AndroidManifest.xml:14:25-74
34            </intent-filter>
35        </receiver>
36    </application>
37
38</manifest>
