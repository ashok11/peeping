import 'package:dio/dio.dart';
import 'package:retrofit/retrofit.dart';
import 'package:video_filter/constant.dart';
import 'package:video_filter/models/success.dart';

part 'api.network.g.dart';


@RestApi(baseUrl: BASE_URL)
abstract class RestClient {
  factory RestClient(Dio dio) = _RestClient;

  @POST("/peeping/songUpload")
  Future<AccountCreateModel> songUploader(@Body() body);

}