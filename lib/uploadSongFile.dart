import 'dart:io';

import 'package:dio/dio.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_filter/methods/selectSongUrl.dart';

class UploadSongFilePath extends StatefulWidget {
  @override
  _UploadSongFilePathState createState() => _UploadSongFilePathState();
}

class _UploadSongFilePathState extends State<UploadSongFilePath> {
  Map<String, String> filesPaths;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text("Upload File"),
        ),
        body: Container(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                RaisedButton(
                  onPressed: () {
                    FocusScope.of(context).requestFocus(FocusNode());
                    _selectSong(filesPaths);
                  },
                  child: Text("Upload Song"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future _selectSong(filesPaths) async {
    File file = await FilePicker.getFile(
        type: FileType.custom, allowedExtensions: ['mp3']);

    if (file != null) {
      String fileName = file.path.split('/').last;
      print("file nameeeeeeeeeeeeee");
      print(fileName);
      FormData formData = new FormData.fromMap({
        "file": await MultipartFile.fromFile(file.path, filename: fileName),
        "id": "5fbdfc8d6829e41ce864689b"

      });

      CreationMethods.songUpLoad(formData);
      // var url = imageUploadRes.fileUrl.elementAt(0).location;
    }
    // File tmpFile = File(file.path);
    // print("helo");
    // print(tmpFile);

    // // 5. Get the path to the apps directory so we can save the file to it.
    // final Directory path = await getApplicationDocumentsDirectory();
    // final String fileName = basename(file.path); // Filename without extension
    // final String fileExtension = extension(file.path); // e.g. '.jpg'
    //
    // // 6. Save the file by copying it to the new location on the device.
    // tmpFile = await tmpFile.copy('$path/$fileName$fileExtension');
    //
    // // 7. Optionally, if you want to display the taken picture we need to update the state
    // // Note: Copying and awaiting the file needs to be done outside the setState function.
    // setState(() => _storedImage = tmpFile);
  }
}
