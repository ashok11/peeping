import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:video_filter/peepingMainFile.dart';
import 'package:video_filter/record.dart';
import 'package:video_filter/selectSongToPlay.dart';
import 'package:video_filter/trimVideo.dart';
import 'package:video_filter/uploadSongFile.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    SystemChrome.setEnabledSystemUIOverlays([]);

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SafeArea(
        top: true,
        child: MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override


  int index = 1;

  changeIndex(index){
    setState(() {
      index = index;
    });
    if(index == 0){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => UploadSongFilePath()),
      );
    }else  if(index == 1){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => RecordButton()),
      );
    }else  if(index == 2){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SelectSongToPlay()),
      );
    }
    else  if(index == 3){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => TrimVideoMaker()),
      );
    }
  }

  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('Peeping App'),
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap:(i) => {
          changeIndex(i)
        } ,
        currentIndex: index, // this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home, color: Colors.black),
            title: new Text('',
              style: TextStyle(
                  color: Colors.black
              ),
            ),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.record_voice_over, color: Colors.black),
            title: new Text('',
              style: TextStyle(
                  color: Colors.black
              ),
            ),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person, color: Colors.black,),
              title: Text('',
                style: TextStyle(
                    color: Colors.black
                ),
              )
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.track_changes, color: Colors.black),
              title: Text('')
          )
        ],
      ),

    );

    // return Scaffold(
    //   body: Container(
    //     width: MediaQuery.of(context).size.width,
    //     height: MediaQuery.of(context).size.height,
    //     child: PeepingMainFile(),
    //   ),
    //
    // );
  }
}
