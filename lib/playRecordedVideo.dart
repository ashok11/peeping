import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_ffmpeg/flutter_ffmpeg.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart' as PackagageVideoPlayer;

class PlayRecordedVideo extends StatefulWidget {
  final String path;
  final double speed;

  PlayRecordedVideo({@required this.path, @required this.speed});

  @override
  _PlayRecordedVideoState createState() => _PlayRecordedVideoState();
}

class _PlayRecordedVideoState extends State<PlayRecordedVideo> {
  bool load = false;
  FlutterFFmpeg fFmpeg;
  PackagageVideoPlayer.VideoPlayerController _controller;

  File fileInfo;
  final spinkit = SpinKitChasingDots(
    color: Color(0xFFee5253),
    size: 50.0,
  );

  void getVideo() async {
    print(widget.speed.toString() + "/////////////////////////////////");
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Oyindori/Filtered';
    await Directory(dirPath).create(recursive: true);
    final String filePath =
        '$dirPath/${DateTime.now().millisecondsSinceEpoch.toString()}';

//          await fFmpeg.execute('-i ' +
//              widget.path +
//              ' -r 1 -f image ' +
//              filePath +
//              '-image-%3d.jpeg').then((rc) => print("FFmpeg process exited with rc $rc"));
    var fi =
        "https://www.learningcontainer.com/wp-content/uploads/2020/02/Kalimba-online-audio-converter.com_-1.wav";
    await fFmpeg.execute("-i "+ widget.path + " -filter_complex [0:v]setpts="+ widget.speed.toString() +"*PTS[v];[0:a]atempo=2.0[a] -map [v] -map [a] " + filePath + "-output.mp4")
        .then((return_code) => print("11111111111111111111111111111111111111111111 $return_code"));

    await fFmpeg
        .execute("-i " + filePath + "-output.mp4" +
            " -i " +
            fi +
            " -c:v copy -c:a aac -map 0:v:0 -map 1:a:0 -shortest " +
            filePath +
            "-output2.mp4")
        .then((return_code) => print("2222222222222222222222222222222222222222222 $return_code"));

    _controller = PackagageVideoPlayer.VideoPlayerController.file(
        File(filePath + '-output2.mp4'))
      ..initialize().then((_) {
        setState(() {
          load = false;
          _controller.play();
          _controller.setLooping(true);
        });
      });

//    fileInfo = File(widget.path);
//    _controller = PackagageVideoPlayer.VideoPlayerController.file(fileInfo)
//      ..initialize().then((_) {
//        setState(() {
//          _controller.play();
//          _controller.setLooping(true);
//        });
//      });
  }

  speedVideo() async {
    final Directory extDir = await getApplicationDocumentsDirectory();
    final String dirPath = '${extDir.path}/Oyindori/Filtered';
    await Directory(dirPath).create(recursive: true);
    final String filePath =
        '$dirPath/${DateTime.now().millisecondsSinceEpoch.toString()}';
    load = true;
  }

  @override
  void initState() {
    super.initState();
    getVideo();
    fFmpeg = new FlutterFFmpeg();
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
     body: Container(
       color: Colors.white,
       height: double.infinity,
       child: _controller == null
           ? spinkit
           : _controller.value.initialized
           ? GestureDetector(
         onTap: () {
           if (_controller.value.isPlaying) {
             _controller.pause();
             _controller.setVolume(0);
           } else {
             _controller.play();
             _controller.setVolume(10);
           }
         },
         child: Container(
           width: MediaQuery.of(context).size.width,
           height: MediaQuery.of(context).size.height,
           child: Stack(
             children: [
               _videoPlayer(),
               IconButton(
                 icon: Icon(
                   Icons.close,
                   color: Colors.white,
                   size: 24,
                 ),
                 onPressed: () {
                   // do something
                   Navigator.pop(context);
                 },
               ),
               Positioned(
                   bottom: 2,
                   child: Row(
                     children: [
                       Card(
                         color: Colors.blue[1 * 100],
                         child: new Container(
                           width: 50.0,
                           height: 50.0,
                           child: _Filters(6.5),
                         ),
                       ),
                       Card(
                         color: Colors.blue[1 * 100],
                         child: new Container(
                           width: 50.0,
                           height: 50.0,
                           child: _Filters(0.5),
                         ),
                       ),
                       Card(
                         color: Colors.blue[1 * 100],
                         child: new Container(
                           width: 50.0,
                           height: 50.0,
                           child: _Filters(-8),
                         ),
                       ),
                     ],
                   ))
             ],
           ),
         ),
       )
           : spinkit,
     ),
    );
  }

  _videoPlayer() {
    return PackagageVideoPlayer.VideoPlayer(
      _controller,
    );
  }

  _Filters(degree) {
    load = true;
    return Container(
      width: MediaQuery.of(context).size.width * 0.3,
      child: RaisedButton(
        onPressed: () async {},
      ),
    );
  }
}
