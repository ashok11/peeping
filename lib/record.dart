// import 'dart:async';
// import 'dart:io';
// import 'dart:math';
// import 'dart:ui';
//
// import 'package:camera/camera.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:lamp/lamp.dart';
// import 'package:path_provider/path_provider.dart';
// import 'package:video_filter/playRecordedVideo.dart';
// import 'package:flutter_playout/audio.dart';
// import 'package:audioplayer/audioplayer.dart';
// import 'package:flutter_ffmpeg/flutter_ffmpeg.dart';
// import 'package:customtogglebuttons/customtogglebuttons.dart';
// import 'package:provider/provider.dart';
// import 'package:video_filter/repo.camera.dart';
//
//
// class Video extends StatefulWidget {
//
//   const Video({Key key}) : super(key: key);
//
//   @override
//   _VideoState createState() => _VideoState();
// }
//
// class _VideoState extends State<Video> {
//   CameraController controller;
//   List<CameraDescription> cameras;
//   bool cameraInit;
//   int cameraIndex = 0;
//
//
//
//   @override
//   Widget build(BuildContext context) {
//
//
//
//     //initCamera(context);
//
//
//     if (!cameraInit) {
//       return Container(
//         color: Colors.black,
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.center,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [CircularProgressIndicator()],
//         ),
//       );
//     }
//     return AspectRatio(
//       aspectRatio: controller.value.aspectRatio,
//       child: Stack(
//         children: [
//           CameraPreview(controller),
//           Container(
//               width: MediaQuery.of(context).size.width,
//               child: RecordButton(
//                 controller: controller,
//                 cameras: cameras,
//                 // callBack:(val){
//                 //   print("11111 $val");
//                 //   setState(() {
//                 //     // cameraIndexToggle(val);
//                 //     // print("222222 $val");
//                 //     // cameraIndex[val] = !cameraIndex[val];
//                 //      cameraIndex = val;
//                 //      if(cameraIndex == 0){
//                 //        setState(() {
//                 //          print("manns");
//                 //          cameraIndex = 1;
//                 //          print(cameraIndex);
//                 //        });
//                 //      }
//                 //      else{
//                 //        setState(() {
//                 //         cameraIndex =0;
//                 //        });
//                 //      }
//                 //   });
//                 //   print("cameraIndex valueee coming");
//                 //    print(cameraIndex);
//                 // },
//
//               )),
//         ],
//       ),
//     );
//   }
//
//
//   Future<void> initCamera(BuildContext context) async {
//     print("manish");
//
//     // final dataProvider = Provider.of<DataProviderBloc>(context);
//     // var cam = dataProvider.getloader;
//
//     cameras = await availableCameras();
//     print("cameras avaliable");
//     print(cameras);
//     availableCameras().then((value) {
//       cameras = value;
//       controller = CameraController(cameras[0], ResolutionPreset.high);
//       print("upper value");
//       print(CameraController(cameras[cameraIndex], ResolutionPreset.high));
//       controller.initialize().then((_) {
//         if (!mounted) {
//           return;
//         }
//         setState(() {
//           cameraInit = true;
//         });
//       });
//     }
//     ).catchError((onError) {
//       print(onError);
//     });
//   }
//
//   @override
//   void initState() {
//
//     super.initState();
//     cameraInit = false;
//      initCamera(context);
//
//   }
//
//   @override
//   void dispose() {
//     controller?.dispose();
//     super.dispose();
//   }
//
//   void selectFilter() {
//     setState(() {});
//   }
//
//
//
//
// }
//
// class RecordButton extends StatefulWidget {
//   final CameraController controller;
//   // final Function callBack;
//   List<CameraDescription> cameras;
//
//   RecordButton({@required this.controller, @required this.cameras});
//
//   @override
//   _RecordButtonState createState() => _RecordButtonState();
// }
// bool buttonDisapper = true;
//
// const kUrl =
//     "https://www.learningcontainer.com/wp-content/uploads/2020/02/Kalimba-online-audio-converter.com_-1.wav";
// enum PlayerState { stopped, playing, paused }
//
// class _RecordButtonState extends State<RecordButton>
//     with TickerProviderStateMixin {
//   List<bool> _isSelected = [false, false, true, false, false];
//
//   List<bool> _isSelectedlap = [false, false, true, false, false];
//   double percentage = 0.0;
//   double newPercentage = 0.0;
//   double videoTime = 0.0;
//   String videoPath;
//   Timer timer;
//   bool speed_select = false;
//   bool timerSelect = false;
//   double speed = 1.0;
//
//
//   Duration duration;
//   Duration position;
//   AudioPlayer audioPlayer;
//   String localFilePath;
//   PlayerState playerState = PlayerState.stopped;
//
//   get isPlaying => playerState == PlayerState.playing;
//
//   get isPaused => playerState == PlayerState.paused;
//
//   get durationText =>
//       duration != null ? duration.toString().split('.').first : '';
//
//   get positionText =>
//       position != null ? position.toString().split('.').first : '';
//
//   bool isMuted = false;
//   bool isRec = false;
//   StreamSubscription _positionSubscription;
//   StreamSubscription _audioPlayerStateSubscription;
//
//   AnimationController percentageAnimationController;
//
//   bool _hasFlash = false;
//   bool _isOn = false;
//   double _intensity = 1.0;
//
//
//   @override
//   void initState() {
//
//     super.initState();
//     setState(() {
//       initAudioPlayer();
//       percentage = 0.0;
//     });
//     initPlatformState();
//     percentageAnimationController = new AnimationController(
//         vsync: this, duration: new Duration(seconds: 1000))
//       ..addListener(() {
//         setState(() {
//           percentage = lerpDouble(
//               percentage, newPercentage, percentageAnimationController.value);
//         });
//       });
//   }
//
//   initPlatformState() async {
//     bool hasFlash = await Lamp.hasLamp;
//     print("Device has flash ? $hasFlash");
//     setState(() { _hasFlash = hasFlash; });
//   }
//
//   String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();
//
//   void onVideoRecordButtonPressed() {
//     startVideoRecording().then((String filePath) {
//       if (mounted) setState(() {});
//       if (filePath != null) print('Saving video to $filePath');
//     });
//   }
//
//   void initAudioPlayer() {
//     audioPlayer = AudioPlayer();
//     _positionSubscription = audioPlayer.onAudioPositionChanged
//         .listen((p) => setState(() => position = p));
//     _audioPlayerStateSubscription =
//         audioPlayer.onPlayerStateChanged.listen((s) {
//       if (s == AudioPlayerState.PLAYING) {
//         setState(() => duration = audioPlayer.duration);
//       } else if (s == AudioPlayerState.STOPPED) {
//         onComplete();
//         setState(() {
//           position = duration;
//         });
//       }
//     }, onError: (msg) {
//       setState(() {
//         playerState = PlayerState.stopped;
//         duration = Duration(seconds: 0);
//         position = Duration(seconds: 0);
//       });
//     });
//   }
//
//   void onComplete() {
//     setState(() => playerState = PlayerState.stopped);
//   }
//
//   Future play() async {
//     setState(() {
//       playerState = PlayerState.playing;
//     });
//     await audioPlayer.play(kUrl);
//   }
//
//   Future<String> startVideoRecording() async {
//     await audioPlayer.play(kUrl).then((value) => {
//           setState(() {
//             playerState = PlayerState.playing;
//           })
//         });
//
//     if (!widget.controller.value.isInitialized) {
//       return null;
//     }
//
//     final Directory extDir = await getApplicationDocumentsDirectory();
//     final String dirPath = '${extDir.path}/Movies/flutter_test';
//     await Directory(dirPath).create(recursive: true);
//     final String filePath = '$dirPath/${timestamp()}.mp4';
//
//     if (widget.controller.value.isRecordingVideo) {
//       // A recording is already started, do nothing.
//       return null;
//     }
//
//     try {
//       setState(() {
//         videoPath = filePath;
//       });
//       await widget.controller.startVideoRecording(filePath);
//     } on CameraException catch (e) {
//       return null;
//     }
//     return filePath;
//   }
//
//   Future<void> stopVideoRecording() async {
//     if (!widget.controller.value.isRecordingVideo) {
//       return null;
//     }
//
//     try {
//       await widget.controller.stopVideoRecording();
//     } on CameraException catch (e) {
//       return null;
//     }
//   }
//
//   playVideo() {
//     return Container(
//       child: InkWell(
//         onTap: () => {
//           Navigator.push(
//             context,
//             CupertinoPageRoute(
//               builder: (_) => PlayRecordedVideo(
//                 path: videoPath,
//                 speed: speed,
//               ),
//             ),
//           ),
//         },
//         child: Text("Done"),
//       ),
//     );
//   }
//
//
//   Future<int> lenghOfAudio() async {
//     final FlutterFFprobe _flutterFFprobe = new FlutterFFprobe();
//     Map info = await _flutterFFprobe.getMediaInformation(kUrl);
// // the given duration is in milliseconds, assuming you want to have seconds I divide by 1000
//     int seconds = (info['duration']).round();
//     print('DURATION: ${seconds.toString()}');
//     return seconds;
//   }
//
//   Timer _timer;
//   int _start = 5;
//   int lap = 3;
//
//
//    startTimer() {
//     if (_timer != null) {
//       _timer.cancel();
//       _timer = null;
//     } else {
//       _timer = new Timer.periodic(
//         const Duration(seconds: 1),
//             (Timer timer) => setState(
//               () {
//             if (lap < 1) {
//               startVideoRecording();
//               print(lap);
//               // startVideoRecording();
//                timer.cancel();
//             } else {
//               lap = lap - 1;
//             }
//           },
//         ),
//       );
//     }
//   }
//
//   @override
//   void dispose() {
//     _timer.cancel();
//     super.dispose();
//   }
//
//
//   @override
//   Widget build(BuildContext context) {
//
//
//     return SafeArea(
//       child: Stack(
//         children: [
//           IconButton(
//             icon: Icon(
//               Icons.close,
//               color: Colors.white,
//               size: 24,
//             ),
//             onPressed: () {
//               // do something
//               Navigator.pop(context);
//             },
//           ),
//
//           Center(
//             child: Container(
//               child: Text("$lap"),
//             ),
//           ),
//
//           Positioned(
//             top: 10,
//             right: 10,
//             child: Column(
//               children: [
//                 Column(children: [
//                   Container(
//                     margin: EdgeInsets.only(top: 5),
//                     width: 25.0,
//                     height: 25.00,
//                     child: Container(
//                       width: MediaQuery.of(context).size.width * 0.3,
//                       child: RaisedButton(
//                         onPressed: () {
//                           final dataProvider = Provider.of<DataProviderBloc>(context);
//                         var cam = dataProvider.getloader;
//
//                         if(cam == 0){
//                           dataProvider.setLoader = 1;
//                         }else{
//                           dataProvider.setLoader = 0;
//                         }
//                          //  setState(() {
//                          //    widget.callBack(0);
//                          //    // Video(key: widget.callBack(0));
//                          //  });
//                          // Video();
//
//                         },
//                       ),
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(top: 6, bottom: 8),
//                     child: Text("Flip", style: TextStyle(color: Colors.white)),
//                   )
//                 ]),
//                 Column(children: [
//                   Container(
//                     width: 25.0,
//                     height: 25.0,
//                     child: Container(
//                       width: MediaQuery.of(context).size.width * 0.3,
//                       child: RaisedButton(
//                         onPressed: () async {
//                           setState(() {
//                             speed_select = !speed_select;
//                           });
//                         },
//                       ),
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(top: 6, bottom: 8),
//                     child: Text("Speed", style: TextStyle(color: Colors.white)),
//                   )
//                 ]),
//                 Column(children: [
//                   Container(
//                     width: 25.0,
//                     height: 25.0,
//                     child: Container(
//                       width: MediaQuery.of(context).size.width * 0.3,
//                       child: RaisedButton(
//                         onPressed: () async {},
//                       ),
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(top: 6, bottom: 8),
//                     child: Text("Beauty", style: TextStyle(color: Colors.white)),
//                   )
//                 ]),
//                 Column(children: [
//                   Container(
//                     width: 25.0,
//                     height: 25.0,
//                     child: Container(
//                       width: MediaQuery.of(context).size.width * 0.3,
//                       child: RaisedButton(
//                         onPressed: () async {},
//                       ),
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(top: 6, bottom: 8),
//                     child: Text("Filter", style: TextStyle(color: Colors.white)),
//                   )
//                 ]),
//                 Column(children: [
//                   Container(
//                     width: 25.0,
//                     height: 25.0,
//                     child: Container(
//                       width: MediaQuery.of(context).size.width * 0.3,
//                       child: RaisedButton(
//                         onPressed: () async {
//                           setState(() {
//                            timerSelect = !timerSelect;
//                           });
//                         },
//                       ),
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(top: 6, bottom: 8),
//                     child: Text("Timer", style: TextStyle(color: Colors.white)),
//                   )
//                 ]),
//                 Column(children: [
//                   Container(
//                     width: 25.0,
//                     height: 25.0,
//                     child: Container(
//                       width: MediaQuery.of(context).size.width * 0.3,
//                       child: RaisedButton(
//                           onPressed: _turnFlash,
//                       ),
//                     ),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.only(top: 6, bottom: 8),
//                     child: Text("Flash", style: TextStyle(color: Colors.white)),
//                   )
//                 ]),
//               ],
//             ),
//           ),
//           Align(
//             // alignment: Alignment.bottomLeft,
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.end,
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 if (speed_select)
//                   Container(
//                      margin: EdgeInsets.only(bottom: 6),
//                     child: Center(
//                       child: CustomToggleButtons(
//                         unselectedFillColor: Colors.black,
//                         children: <Widget>[
//                           Container(
//                               child: Text("0.3x")),
//                           Container(child: Text("0.5x")),
//                           Container(child: Text("1x")),
//                           Container(child: Text("2x")),
//                           Container(child: Text("3x"))
//                         ],
//
//                         onPressed: (int index) {
//                           setState(() {
//                             switch (index) {
//                               case 0:
//                                 speed = 0.3;
//                                 break;
//                               case 1:
//                                 speed = 0.5;
//                                 break;
//                               case 2:
//                                 speed = 1.0;
//                                 break;
//                               case 3:
//                                 speed = 2.0;
//                                 break;
//                               case 4:
//                                 speed = 3.0;
//                                 break;
//                               default:
//                                 speed = 1.0;
//                                 break;
//                             }
//                             _isSelected = [false, false, false, false, false];
//                             _isSelected[index] = !_isSelected[index];
//                             print(speed);
//                           });
//                         },
//                         isSelected: _isSelected,
//                         // region example 1
//                         color: Colors.white,
//                         selectedColor: Colors.black,
//                         fillColor: Colors.white,
//                         // endregion
//                         // region example 2
//                         borderColor: Colors.black,
//                         disabledColor: Colors.pink,
//                         // borderRadius: 1,
//                       //  selectedBorderColor: Colors.white,
//                       //   borderRadius: BorderRadius.all(Radius.circular(15.0)),
//                         // endregion
//                       ),
//                     ),
//                   )
//                 else
//                   Container(),
//                 if (timerSelect)
//                   Container(
//                     margin: EdgeInsets.only(bottom: 5),
//                     child: Center(
//                       child: ToggleButtons(
//                         children: <Widget>[
//                           Container(child: Text("3")),
//                           Container(child: Text("5")),
//                           Container(child: Text("8")),
//                           Container(child: Text("10")),
//                           Container(child: Text("14"))
//                         ],
//
//                         onPressed: (int index) {
//                           setState(() {
//                             switch (index) {
//                               case 0:
//                                 lap = 3;
//                                 break;
//                               case 1:
//                                 lap = 5;
//                                 break;
//                               case 2:
//                                 lap = 8;
//                                 break;
//                               case 3:
//                                 lap = 10;
//                                 break;
//                               case 4:
//                                 lap = 14;
//                                 break;
//                               default:
//                                 lap = 3;
//                                 break;
//                             }
//                             _isSelectedlap = [false, false, false, false, false];
//                             _isSelectedlap[index] = !_isSelectedlap[index];
//                             print(lap);
//                           });
//                         },
//                         isSelected: _isSelectedlap,
//                         // region example 1
//                         color: Colors.white,
//                         selectedColor: Colors.black,
//                         fillColor: Colors.white,
//                         // endregion
//                         // region example 2
//                         borderColor: Colors.black,
//                         selectedBorderColor: Colors.white,
//                         borderRadius: BorderRadius.all(Radius.circular(10)),
//                         // endregion
//                       ),
//                     ),
//                   )
//                 else
//                   Container(),
//                 Container(
//                   width: MediaQuery.of(context).size.width,
//                   child: CustomPaint(
//                     isComplex: true,
//                     foregroundPainter: RecordButtonPainter(
//                         lineColor: Colors.black12,
//                         // completeColor: Color(0xFFee5253),
//                         completeColor: Colors.red,
//                         completePercent: percentage,
//                         width: 6.0),
//                   ),
//                 ),
//                 Padding(
//                   padding: EdgeInsets.only(left: 15, right: 15),
//                   child: Row(
//                     children: [
//                       Text("data"),
//                       Spacer(),
//                       buttonDisapper != true ?  Container(
//                         height: 100.0,
//                         width: 100.0,
//                       ) :
//                       Container(
//                         height: 100.0,
//                         width: 100.0,
//                         child: Padding(
//                           padding: const EdgeInsets.all(10.0),
//                           child: GestureDetector(
//                             onLongPress: () {
//                               setState(() {
//                                 isRec = true;
//                                 buttonDisapper = true;
//                               });
//                                startTimer();
//                               // startVideoRecording();
//                               timer = new Timer.periodic(
//                                 Duration(seconds: 1),
//                                     (Timer t) => setState(() {
//                                   percentage = newPercentage;
//                                   newPercentage += 1;
//                                   if (newPercentage > 60) {
//                                     percentage = 0.0;
//                                     newPercentage = 0.0;
//                                     timer.cancel();
//                                     stopVideoRecording();
//                                     setState(() {
//                                       audioPlayer.stop();
//                                       buttonDisapper = false;
//                                     });
//
//                                     playVideo();
//                                   }
//                                   percentageAnimationController.forward(
//                                       from: 0.0);
//                                   // print((t.tick / 1000).toStringAsFixed(0));
//                                 }),
//                               );
//                               _audioPlayerStateSubscription =
//                                   audioPlayer.onPlayerStateChanged.listen((s) {
//                                     if (isRec && s == AudioPlayerState.PLAYING) {}
//                                   });
//                             },
//                             onLongPressEnd: (e) {
//                               setState(() {
//                                 isRec = false;
//                                 audioPlayer.stop();
//                               });
//
//                               percentage = 0.0;
//                               newPercentage = 0.0;
//                               timer.cancel();
//                               stopVideoRecording();
//                               playVideo();
//                             },
//                             child: Container(
//                               child: Center(
//                                 child: new Text(
//                                   "Hold",
//                                   style: TextStyle(
//                                     color: Colors.white,
//                                     fontWeight: FontWeight.bold,
//                                   ),
//                                 ),
//                               ),
//                               decoration: BoxDecoration(
//                                 color: Color(0xFFee5253),
//                                 borderRadius: BorderRadius.circular(50),
//                               ),
//                             ),
//                           ),
//                         ),
//                       ),
//                       Spacer(),
//                       videoPath != null
//                           ? Container(
//                               child: playVideo(),
//                             )
//                           : Container()
//                     ],
//                   ),
//                 )
//               ],
//             ),
//           )
//         ],
//       ),
//     );
//   }
//   Future _turnFlash() async {
//     _isOn ? Lamp.turnOff() : Lamp.turnOn(intensity: _intensity);
//     var f = await Lamp.hasLamp;
//     setState((){
//       _hasFlash = f;
//       _isOn = !_isOn;
//     });
//   }
//
//
//   int selectedCameraIdx;
//
//
//
// }
//
// class RecordButtonPainter extends CustomPainter {
//   Color lineColor;
//   Color completeColor;
//   double completePercent;
//   double width;
//
//   RecordButtonPainter(
//       {this.lineColor, this.completeColor, this.completePercent, this.width});
//
//   @override
//   void paint(Canvas canvas, Size size) {
//     Paint line = Paint()
//       ..color = lineColor
//       ..strokeCap = StrokeCap.round
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = width;
//     Paint complete = Paint()
//       ..color = completeColor
//       ..strokeCap = StrokeCap.round
//       ..style = PaintingStyle.stroke
//       ..strokeWidth = width;
//
//     // Offset start = Offset(-130.0, 0.0);
//     // Offset end = Offset(220.0, 0.0);
//     // canvas.drawLine( Offset(0.0, 0.0), Offset(10.0, 0.0), line);
//     // double arcAngle = 2 * pi * (completePercent / 9390);
//     // canvas.drawArc(Rect.fromPoints(Offset(-130.0, 0.0), Offset(220.0, 0.0)),  pi / 2, arcAngle, false, complete);
//
//     canvas.drawLine(Offset(0.0, 0.0), Offset(size.width, 0.0), line);
//     double arcAngle = 365 * (completePercent / 60);
//
//     canvas.drawLine(Offset(0.0, 0), Offset(arcAngle, 0), complete);
//   }
//
//   @override
//   bool shouldRepaint(CustomPainter oldDelegate) {
//     return true;
//   }
// }


import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'dart:ui';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lamp/lamp.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_filter/playRecordedVideo.dart';
import 'package:flutter_playout/audio.dart';
import 'package:audioplayer/audioplayer.dart';
import 'package:flutter_ffmpeg/flutter_ffmpeg.dart';
import 'package:customtogglebuttons/customtogglebuttons.dart';
import 'package:provider/provider.dart';
import 'package:torch/torch.dart';



class RecordButton extends StatefulWidget {
  final String url;

  RecordButton({ this.url});

  @override
  _RecordButtonState createState() => _RecordButtonState();
}

bool buttonDisapper = true;

const kUrl = "https://www.learningcontainer.com/wp-content/uploads/2020/02/Kalimba-online-audio-converter.com_-1.wav";

enum PlayerState { stopped, playing, paused }

class _RecordButtonState extends State<RecordButton> with TickerProviderStateMixin {

  List<CameraDescription> cameras;
  bool cameraInit;
  int cameraIndex = 0;
  CameraController controller;

  List<bool> _isSelected = [false, false, true, false, false];
  List<bool> _isSelectedlap = [false, true, false];

  double percentage = 0.0;
  double newPercentage = 0.0;
  double videoTime = 0.0;
  String videoPath;
  Timer timer;
  bool speed_select = false;
  bool timerSelect = false;
  double speed = 1.0;


  Duration duration;
  Duration position;
  AudioPlayer audioPlayer;
  String localFilePath;
  PlayerState playerState = PlayerState.stopped;

  get isPlaying => playerState == PlayerState.playing;

  get isPaused => playerState == PlayerState.paused;

  get durationText => duration != null ? duration.toString().split('.').first : '';

  get positionText => position != null ? position.toString().split('.').first : '';

  bool isMuted = false;
  bool isRec = false;
  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;

  AnimationController percentageAnimationController;

  //For Flash
  bool _hasFlash = true;
  bool _isOn = true;
  double _intensity = 1.0;

bool newShow = true;
bool timerShow = true;


  @override
  void initState() {

    cameraInit = false;
    initCamera(0);
    super.initState();
    setState(() {
      initAudioPlayer();
      percentage = 0.0;
    });
    percentageAnimationController = new AnimationController(
        vsync: this, duration: new Duration(seconds: 1000))
      ..addListener(() {
        setState(() {
          Timer(Duration(seconds: lap), () =>
          percentage = lerpDouble(
              percentage, newPercentage, percentageAnimationController.value)
          );
          // percentage = lerpDouble(
          //     percentage, newPercentage, percentageAnimationController.value);
        });
      });
  }

  Future<void> initCamera(int val) async {
    availableCameras().then((value) {
      cameras = value;
      controller = CameraController(cameras[val], ResolutionPreset.high);
      controller.initialize().then((_) {
        if (!mounted) {
          return;
        }
        setState(() {
          cameraInit = true;
        });
      });
    }).catchError((onError) {
      print(onError);
    });
  }


  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  void onVideoRecordButtonPressed() {
    startVideoRecording().then((String filePath) {
      if (mounted) setState(() {});
      if (filePath != null) print('Saving video to $filePath');
    });
  }

  void initAudioPlayer() {
    audioPlayer = AudioPlayer();
    _positionSubscription = audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => position = p));
    _audioPlayerStateSubscription =
        audioPlayer.onPlayerStateChanged.listen((s) {
          if (s == AudioPlayerState.PLAYING) {
            setState(() => duration = audioPlayer.duration);
          } else if (s == AudioPlayerState.STOPPED) {
            onComplete();
            setState(() {
              position = duration;
            });
          }
        }, onError: (msg) {
          setState(() {
            playerState = PlayerState.stopped;
            duration = Duration(seconds: 0);
            position = Duration(seconds: 0);
          });
        });
  }

  void onComplete() {
    setState(() => playerState = PlayerState.stopped);
  }

  Future play() async {
    setState(() {
      playerState = PlayerState.playing;
    });
    await audioPlayer.play(widget.url ?? kUrl );
  }

  Future<String> startVideoRecording() async {
    await audioPlayer.play(widget.url ?? kUrl).then((value) => {
      setState(() {
        playerState = PlayerState.playing;
      })
    });
if(playerState.toString() == 'playing'){
  if (!controller.value.isInitialized) {
    return null;
  }

  final Directory extDir = await getApplicationDocumentsDirectory();
  final String dirPath = '${extDir.path}/Movies/flutter_test';
  await Directory(dirPath).create(recursive: true);
  final String filePath = '$dirPath/${timestamp()}.mp4';

  if (controller.value.isRecordingVideo) {
    // A recording is already started, do nothing.
    return null;
  }

  try {
    setState(() {
      videoPath = filePath;
    });
    await controller.startVideoRecording(filePath);
  } on CameraException catch (e) {
    return null;
  }
  return filePath;
}

  }

  Future<void> stopVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.stopVideoRecording();
    } on CameraException catch (e) {
      return null;
    }
  }

  playVideo() {
    return Container(
      child: InkWell(
        onTap: () => {
          Navigator.push(
            context,
            CupertinoPageRoute(
              builder: (_) => PlayRecordedVideo(
                path: videoPath,
                speed: speed,
              ),
            ),
          ),
        },
        child: Text("Done"),
      ),
    );
  }


  Future<int> lenghOfAudio() async {
    final FlutterFFprobe _flutterFFprobe = new FlutterFFprobe();
    Map info = await _flutterFFprobe.getMediaInformation(widget.url ?? kUrl);
// the given duration is in milliseconds, assuming you want to have seconds I divide by 1000
    int seconds = (info['duration']).round();
    print('DURATION: ${seconds.toString()}');
    return seconds;
  }

  Timer _timer;
  int _start = 5;
  int lap = 0;


  startTimer() {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    } else {
      _timer = new Timer.periodic(
        const Duration(seconds: 1),
            (Timer timer) => setState(
              () {
            if (lap < 1) {
              startVideoRecording();
              print(lap);
              // startVideoRecording();
              timer.cancel();
            } else {
              lap = lap - 1;
            }
          },
        ),
      );
    }
  }

  @override
  void dispose() {
    controller?.dispose();
    _timer.cancel();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    if (!cameraInit) {
      return Container(
        color: Colors.black,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [CircularProgressIndicator()],
        ),
      );
    }
    return Scaffold(
      body: Stack(
        children: [
          Container(
            child: CameraPreview(controller),
          ),
          Container(
              width: MediaQuery.of(context).size.width,
              child: SafeArea(
                child: Stack(
                  children: [
                    IconButton(
                      icon: Icon(
                        Icons.close,
                        color: Colors.white,
                        size: 24,
                      ),
                      onPressed: () {
                        // do something
                        Navigator.pop(context);
                      },
                    ),
                   timerShow == false ? Center(
                      child: Container(
                        child: Text("$lap"),
                      ),
                    ) : Container(),
                    Positioned(
                      top: 10,
                      right: 10,
                      child: Column(
                        children: [
                          Column(children: [
                            Container(
                              margin: EdgeInsets.only(top: 5),
                              width: 25.0,
                              height: 25.00,
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.3,
                                child: RaisedButton(
                                  onPressed: () {
                                    setState(() {
                                      if(cameraIndex == 0){
                                        cameraIndex=1;
                                      }else{
                                        cameraIndex=0;
                                      }
                                      availableCameras().then((value) {
                                        cameras = value;
                                        controller = CameraController(cameras[cameraIndex], ResolutionPreset.high);
                                        controller.initialize().then((_) {
                                          if (!mounted) {
                                            return;
                                          }
                                          setState(() {
                                            cameraInit = true;
                                          });
                                        });
                                      }).catchError((onError) {
                                        print(onError);
                                      });

                                      print(cameraIndex);
                                    });
                                  },
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6, bottom: 8),
                              child: Text("Flip", style: TextStyle(color: Colors.white)),
                            )
                          ]),
                          Column(children: [
                            Container(
                              width: 25.0,
                              height: 25.0,
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.3,
                                child: RaisedButton(
                                  onPressed: () async {
                                    setState(() {
                                      speed_select = !speed_select;
                                    });
                                  },
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6, bottom: 8),
                              child: Text("Speed", style: TextStyle(color: Colors.white)),
                            )
                          ]),
                          Column(children: [
                            Container(
                              width: 25.0,
                              height: 25.0,
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.3,
                                child: RaisedButton(
                                  onPressed: () async {},
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6, bottom: 8),
                              child: Text("Beauty", style: TextStyle(color: Colors.white)),
                            )
                          ]),
                          Column(children: [
                            Container(
                              width: 25.0,
                              height: 25.0,
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.3,
                                child: RaisedButton(
                                  onPressed: () async {},
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6, bottom: 8),
                              child: Text("Filter", style: TextStyle(color: Colors.white)),
                            )
                          ]),
                          Column(children: [
                            Container(
                              width: 25.0,
                              height: 25.0,
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.3,
                                child: RaisedButton(
                                  onPressed: () async {
                                    setState(() {
                                      timerSelect = !timerSelect;

                                    });
                                  },
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6, bottom: 8),
                              child: Text("Timer", style: TextStyle(color: Colors.white)),
                            )
                          ]),
                          Column(children: [
                            Container(
                              width: 25.0,
                              height: 25.0,
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.3,
                                child: RaisedButton(
                                  onPressed: () {
                                    flashCamera();
                                  },
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 6, bottom: 8),
                              child: Text("Flash", style: TextStyle(color: Colors.white)),
                            )
                          ]),
                        ],
                      ),
                    ),
                    Align(
                      // alignment: Alignment.bottomLeft,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          if (speed_select)
                            Container(
                              margin: EdgeInsets.only(bottom: 6),
                              child: Center(
                                child: ToggleButtons(
                                  children: <Widget>[
                                    Container(
                                        child: Text("0.3x")),
                                    Container(child: Text("0.5x")),
                                    Container(child: Text("1x")),
                                    Container(child: Text("2x")),
                                    Container(child: Text("3x"))
                                  ],

                                  onPressed: (int index) {
                                    setState(() {
                                      switch (index) {
                                        case 0:
                                          speed = 0.3;
                                          break;
                                        case 1:
                                          speed = 0.5;
                                          break;
                                        case 2:
                                          speed = 1.0;
                                          break;
                                        case 3:
                                          speed = 2.0;
                                          break;
                                        case 4:
                                          speed = 3.0;
                                          break;
                                        default:
                                          speed = 1.0;
                                          break;
                                      }
                                      _isSelected = [false, false, false, false, false];
                                      _isSelected[index] = !_isSelected[index];
                                      print(speed);
                                    });
                                  },
                                  isSelected: _isSelected,
                                  // region example 1
                                  // region example 1
                                  color: Colors.white,
                                  selectedColor: Colors.black,
                                  fillColor: Colors.white,
                                  // endregion
                                  // region example 2
                                  borderColor: Colors.black,
                                  selectedBorderColor: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  // endregion
                                ),
                              ),
                            )
                          else
                            Container(),
                          if (timerSelect)
                            Container(
                              margin: EdgeInsets.only(bottom: 5),
                              child: Center(
                                child: ToggleButtons(
                                  children: <Widget>[
                                    Container(child: Text("3s")),
                                    Container(child: Text("5s")),
                                    Container(child: Text("10s")),
                                  ],

                                  onPressed: (int index) {
                                    setState(() {
                                      timerShow = false;
                                      switch (index) {
                                        case 0:
                                          lap = 3;
                                          break;
                                        case 1:
                                          lap = 5;
                                          break;
                                        case 2:
                                          lap = 10;
                                          break;
                                        default:
                                          lap = 3;
                                          break;
                                      }
                                      _isSelectedlap = [false, false, false];
                                      _isSelectedlap[index] = !_isSelectedlap[index];
                                      print(lap);
                                    });
                                  },
                                  isSelected: _isSelectedlap,

                                  // region example 1
                                  color: Colors.white,
                                  selectedColor: Colors.black,
                                  fillColor: Colors.white,
                                  // endregion
                                  // region example 2
                                  borderColor: Colors.black,
                                  selectedBorderColor: Colors.white,
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  // endregion
                                ),
                              ),
                            )
                          else
                            Container(),
                          Container(
                            width: MediaQuery.of(context).size.width,
                            child: CustomPaint(
                              isComplex: true,
                              foregroundPainter: RecordButtonPainter(
                                  lineColor: Colors.black12,
                                  // completeColor: Color(0xFFee5253),
                                  completeColor: Colors.red,
                                  completePercent: percentage,
                                  width: 6.0),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 15, right: 15),
                            child: Row(
                              children: [
                                Container(
                                  width: 25.0,
                                  height: 25.0,
                                  child: Container(
                                    width: MediaQuery.of(context).size.width * 0.3,
                                    child: RaisedButton(
                                      onPressed: () async {
                                        setState(() {
                                        });
                                      },
                                      // child: Icon(Icons.priority_high),
                                    ),
                                  ),
                                ),
                                Spacer(),
                                buttonDisapper != true ?  Container(
                                  height: 100.0,
                                  width: 100.0,
                                ) :
                                Container(
                                  height: 100.0,
                                  width: 100.0,
                                  child: Padding(
                                    padding: const EdgeInsets.all(10.0),
                                    child: GestureDetector(
                                      onLongPress: () {
                                        setState(() {
                                          isRec = true;
                                          buttonDisapper = true;
                                          newShow = false;
                                        });
                                        startTimer();
                                        // startVideoRecording();
                                        timer = new Timer.periodic(
                                          Duration(seconds: 1),
                                              (Timer t) => setState(() {
                                            percentage = newPercentage;
                                            newPercentage += 1;
                                            if (newPercentage > 60) {
                                              percentage = 0.0;
                                              newPercentage = 0.0;
                                              timer.cancel();
                                              stopVideoRecording();
                                              setState(() {
                                                audioPlayer.stop();
                                                buttonDisapper = false;
                                              });

                                              playVideo();
                                            }
                                            percentageAnimationController.forward(
                                                from: 0.0);
                                            // print((t.tick / 1000).toStringAsFixed(0));
                                          }),
                                        );
                                        _audioPlayerStateSubscription =
                                            audioPlayer.onPlayerStateChanged.listen((s) {
                                              if (isRec && s == AudioPlayerState.PLAYING) {}
                                            });
                                      },
                                      onLongPressEnd: (e) {
                                        setState(() {
                                          isRec = false;
                                          audioPlayer.stop();
                                        });

                                        percentage = 0.0;
                                        newPercentage = 0.0;
                                        timer.cancel();
                                        stopVideoRecording();
                                        playVideo();
                                      },
                                      child: Container(
                                        child: Center(
                                          child: new Text(
                                            "Hold",
                                            style: TextStyle(
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        decoration: BoxDecoration(
                                          color: Color(0xFFee5253),
                                          borderRadius: BorderRadius.circular(50),
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Spacer(),
                                newShow == false
                                    ? Container(
                                  child: playVideo(),
                                )
                                    : Container()
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              )),
        ],
      ),
    );

  }


  bool _flashShow = true;

  void flashCamera() {
    setState(() {
     if(_flashShow == true){
       Torch.flashOn;
       _flashShow = false;
     }else{
       Torch.flashOff;
       _flashShow = true;
     }
    });
  }

}

class RecordButtonPainter extends CustomPainter {
  Color lineColor;
  Color completeColor;
  double completePercent;
  double width;

  RecordButtonPainter(
      {this.lineColor, this.completeColor, this.completePercent, this.width});

  @override
  void paint(Canvas canvas, Size size) {
    Paint line = Paint()
      ..color = lineColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = width;
    Paint complete = Paint()
      ..color = completeColor
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.stroke
      ..strokeWidth = width;

    // Offset start = Offset(-130.0, 0.0);
    // Offset end = Offset(220.0, 0.0);
    // canvas.drawLine( Offset(0.0, 0.0), Offset(10.0, 0.0), line);
    // double arcAngle = 2 * pi * (completePercent / 9390);
    // canvas.drawArc(Rect.fromPoints(Offset(-130.0, 0.0), Offset(220.0, 0.0)),  pi / 2, arcAngle, false, complete);

    canvas.drawLine(Offset(0.0, 0.0), Offset(size.width, 0.0), line);
    double arcAngle = 365 * (completePercent / 60);

    canvas.drawLine(Offset(0.0, 0), Offset(arcAngle, 0), complete);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}