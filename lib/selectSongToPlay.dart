import 'package:flutter/material.dart';
import 'package:video_filter/methods/selectSongUrl.dart';
import 'package:video_filter/record.dart';
import 'models/selectSong.dart';
import 'dart:convert';

class SelectSongToPlay extends StatefulWidget {
  @override
  _SelectSongToPlayState createState() => _SelectSongToPlayState();
}

class _SelectSongToPlayState extends State<SelectSongToPlay> {


  var country;

@override
  void initState() {
  Future.delayed(Duration.zero, () async {
    final model = await CreationMethods.getPickerJsonModel();
    setState(() {
      country = model;
    });
  });
    // TODO: implement initState

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    // List<Address> list = audioData;



      return Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: Text("Song Select"),
        ),
        body: Center(
          child: ListView.builder(
              scrollDirection: Axis.vertical,
              itemCount: country.address.length,
              itemBuilder: (context, i) {
                // return GestureDetector(
                //   onTap: (){
                //     Navigator.of(context).push(MaterialPageRoute(builder: (context) => RecordButton(url: country.address[i].url,)));
                //   },
                //   child: Container(
                //     child: Column(
                //       mainAxisAlignment: MainAxisAlignment.center,
                //       crossAxisAlignment: CrossAxisAlignment.center,
                //       children: [
                //         Text(country.address[i].title)
                //       ],
                //     )
                //   ),
                // );

                return Padding(
                  padding: EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      InkWell(
                          onTap: () => {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => RecordButton(url: country.address[i].url,)))
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                margin: EdgeInsets.only(left: 17),
                                width: 150,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      country.address[i].title ?? '',
                                      overflow: TextOverflow.ellipsis,
                                      maxLines: 1,
                                      style: TextStyle(
                                          fontFamily: 'CB',
                                          color: Color(0xff21303F),
                                          fontSize: 18,
                                          letterSpacing: 1),
                                    ),

                                  ],
                                ),
                              ),
                              Spacer(),
                              Container(
                                child: Text("Select",
                                    style: TextStyle(
                                        fontFamily: 'CB',
                                        color: Color(0xff21303F),
                                        fontSize: 18,
                                        letterSpacing: 1)
                                ),
                              )
                            ],
                          )
                      ),
                      Divider()
                    ],
                  ),
                );


              }),
        ),
      );




  }
}
