import 'package:dio/dio.dart';


class DioClient {
  static getDio() {
    final dio = Dio();
    dio.options.headers["Content-Type"] = "application/json";
    dio.options.contentType = Headers.formUrlEncodedContentType;
    // TODO: Remove while publishing
//    dio.interceptors.add(PrettyDioLogger());
    return dio;
  }
}