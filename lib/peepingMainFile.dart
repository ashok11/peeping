import 'package:flutter/material.dart';
import 'package:video_filter/record.dart';
import 'package:video_filter/selectSongToPlay.dart';
import 'package:video_filter/trimVideo.dart';
import 'package:video_filter/uploadSongFile.dart';

class PeepingMainFile extends StatefulWidget {
  @override
  _PeepingMainFileState createState() => _PeepingMainFileState();
}

class _PeepingMainFileState extends State<PeepingMainFile> {

  int index = 0;

  changeIndex(index){
    setState(() {
      index = index;
    });
    if(index == 0){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => UploadSongFilePath()),
      );
    }else  if(index == 1){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => RecordButton()),
      );
    }else  if(index == 2){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => SelectSongToPlay()),
      );
    }else  if(index == 3){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => TrimVideoMaker()),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Flutter App'),
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap:(i) => {
          changeIndex(i)
        } ,
        currentIndex: index, // this will be set when a new tab is tapped
        items: [
          BottomNavigationBarItem(
            icon: new Icon(Icons.home, color: Colors.black),
            title: new Text('Upload',
            style: TextStyle(
                color: Colors.black
            ),
            ),
          ),
          BottomNavigationBarItem(
            icon: new Icon(Icons.record_voice_over, color: Colors.black),
            title: new Text('Record',
              style: TextStyle(
                  color: Colors.black
              ),
            ),
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person, color: Colors.black,),
              title: Text('Profile',
                style: TextStyle(
                    color: Colors.black
                ),
              )
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.track_changes, color: Colors.black),
              title: Text('file')
          )
        ],
      ),
    );
    // return Scaffold(
    //   backgroundColor: Colors.white,
    //   body: Column(
    //     mainAxisAlignment: MainAxisAlignment.start,
    //     crossAxisAlignment: CrossAxisAlignment.center,
    //     children: [
    //       Container(
    //         child: RaisedButton(
    //           onPressed: () {
    //
    //             Navigator.of(context).push(MaterialPageRoute(builder: (context) => UploadSongFilePath()));
    //           },
    //           child: Text("Upload Music"),
    //         ),
    //
    //       ),
    //       Container(
    //         child: RaisedButton(
    //           onPressed: () {
    //
    //             Navigator.of(context).push(MaterialPageRoute(builder: (context) => RecordButton()));
    //           },
    //           child: Text("Record Video"),
    //         ),
    //       ),
    //       Container(
    //         child: RaisedButton(
    //           onPressed: () {
    //
    //             Navigator.of(context).push(MaterialPageRoute(builder: (context) => SelectSongToPlay()));
    //           },
    //           child: Text("Sound Add"),
    //         ),
    //       ),
    //       Container(
    //         child: RaisedButton(
    //           onPressed: () {
    //
    //             Navigator.of(context).push(MaterialPageRoute(builder: (context) => TrimVideoMaker()));
    //           },
    //           child: Text("Trim Video"),
    //         ),
    //       )
    //     ],
    //   ),
    // );
  }
}
