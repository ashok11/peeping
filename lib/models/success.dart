import 'dart:convert';

AccountCreateModel accountCreateModelFromJson(String str) => AccountCreateModel.fromJson(json.decode(str));

String accountCreateModelToJson(AccountCreateModel data) => json.encode(data.toJson());


class AccountCreateModel {
  String message;

  AccountCreateModel({this.message});

  AccountCreateModel.fromJson(Map<String, dynamic> json) {
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    return data;
  }
}
