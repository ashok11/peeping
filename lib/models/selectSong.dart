// To parse this JSON data, do
//
//     final songSelectUrl = songSelectUrlFromJson(jsonString);

import 'dart:convert';


SongSelectUrl songSelectUrlFromJson(String str) => SongSelectUrl.fromJson(json.decode(str));

String songSelectUrlToJson(SongSelectUrl data) => json.encode(data.toJson());

class SongSelectUrl {
  SongSelectUrl({
    this.address,
  });

  List<Address> address;

  factory SongSelectUrl.fromJson(Map<String, dynamic> json) => SongSelectUrl(
    address: List<Address>.from(json["address"].map((x) => Address.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "address": List<dynamic>.from(address.map((x) => x.toJson())),
  };
}

class Address {
  Address({
    this.title,
    this.url,
  });

  String title;
  String url;

  factory Address.fromJson(Map<String, dynamic> json) => Address(
    title: json["title"],
    url: json["url"],
  );

  Map<String, dynamic> toJson() => {
    "title": title,
    "url": url,
  };
}
