import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:video_filter/editor.dart';

class TrimVideoMaker extends StatefulWidget {
  @override
  _TrimVideoMakerState createState() => _TrimVideoMakerState();
}

class _TrimVideoMakerState extends State<TrimVideoMaker> {
  _pickVideo() async {
    // File video = await ImagePicker.pickVideo(source: ImageSource.gallery);
    File video = await ImagePicker.pickVideo(source: ImageSource.gallery);
    if(video.path!=null){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Editor(picked: video)),
      );
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text("Trim Video"),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            padding: const EdgeInsets.fromLTRB(10,120,10,0),
            child: Column(
              children: <Widget>[
                Text('Choose a Video to Trim',style: TextStyle(fontSize: 20,color: Colors.grey[600]),),
                SizedBox(height: 70,),
                RaisedButton(
                  padding: EdgeInsets.all(15),
                  color: Colors.red[900],
                  shape: RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(15.0),
                      side: BorderSide(color: Colors.grey[400])
                  ),
                  onPressed: () {
                    _pickVideo();
                  },
                  // child: Icon(Icons.add, color: Colors.white, size: 50,),
                  child: Text("Select", style: TextStyle(fontSize: 20, color: Colors.white),),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
